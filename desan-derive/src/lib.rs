use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;

fn derive_sanitize_debug_impl(input: syn::DeriveInput) -> proc_macro2::TokenStream {
    let ident = &input.ident;

    match &input.data {
        syn::Data::Struct(s) => derive_sanitize_debug_struct(&input, s),
        // syn::Data::Enum(e) => derive_sanitize_debug_enum(&input, e)
        _ => unimplemented!("Cannot derive SanitizeDebug on {}", ident),
    }
}

fn sanitize_field(
    field: &syn::Field,
    getter: proc_macro2::TokenStream,
) -> proc_macro2::TokenStream {
    // extract attribute
    let attribute = field.attrs.iter().find(|attr| {
        if let Some(f) = &attr.path.segments.first() {
            f.ident == "desan"
        } else {
            false
        }
    });

    if let Some(attr) = attribute {
        let attr: syn::Expr = attr.parse_args().expect("attribute syntax");
        let ty = &field.ty;
        let helper = match attr {
            syn::Expr::Call(call) => {
                let func = &call.func;
                let args = call.args.iter();
                quote! {
                    #func(#getter, #(#args,)*)
                }
            }
            _ => panic!("unparsable attribute"),
        };

        quote! {
            &<#ty as desan::Sanitizable<'_, _>>::Sanitized::#helper
        }
    } else {
        getter
    }
}

fn derive_sanitize_debug_struct(
    input: &syn::DeriveInput,
    stru: &syn::DataStruct,
) -> proc_macro2::TokenStream {
    let ident = &input.ident;

    let (s, fields): (_, Vec<proc_macro2::TokenStream>) = match &stru.fields {
        syn::Fields::Unit => (quote!(fmt.debug_tuple(stringify!(#ident))), vec![]),
        syn::Fields::Unnamed(unnamed_fields) => {
            let fields = unnamed_fields
                .unnamed
                .iter()
                .enumerate()
                .map(|(idx, f)| {
                    let field = sanitize_field(f, quote!(&self.#idx));

                    quote! {
                        s.field(#field);
                    }
                })
                .collect();
            (quote!(fmt.debug_tuple(stringify!(#ident))), fields)
        }
        syn::Fields::Named(named_fields) => {
            let fields = named_fields
                .named
                .iter()
                .map(|f| {
                    let name = f.ident.as_ref().unwrap();
                    let field = sanitize_field(f, quote! {&self.#name});

                    quote! {
                        s.field(stringify!(#name), #field);
                    }
                })
                .collect();
            (quote!(fmt.debug_struct(stringify!(#ident))), fields)
        }
    };

    quote! {
        impl desan::SanitizeDebug for #ident {
            fn fmt_sanitized(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                let mut s = #s;
                #(#fields;)*
                s.finish()
            }
        }

        impl std::fmt::Debug for #ident {
            fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                use desan::SanitizeDebug;
                self.fmt_sanitized(fmt)
            }
        }
    }
}

#[proc_macro_derive(SanitizeDebug, attributes(desan))]
pub fn derive_sanitize_debug(item: TokenStream) -> TokenStream {
    derive_sanitize_debug_impl(parse_macro_input!(item)).into()
}
