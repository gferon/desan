use desan_derive::SanitizeDebug;

#[derive(SanitizeDebug)]
struct Foobar {
    #[desan(retain(..4, '*'))]
    username: String,
    #[desan(replace("****"))]
    password: String,
}

#[test]
fn test_foobar() {
    let f = Foobar {
        username: "rubdos".into(),
        password: "very secure".into(),
    };

    assert_eq!(
        format!("{:?}", f),
        r#"Foobar { username: "rubd**", password: "****" }"#
    );
}
