use desan_derive::SanitizeDebug;

#[derive(SanitizeDebug)]
struct Foobar {
    #[desan(retain_first('x'))]
    inner: uuid::Uuid,
}

#[test]
fn test_foobar() {
    let f = Foobar {
        inner: uuid::Uuid::parse_str("d002ba7a-0040-0110-0110-000100010000").unwrap(),
    };

    assert_eq!(
        format!("{:?}", f),
        r#"Foobar { inner: d002ba7a-xxxx-xxxx-xxxx-xxxxxxxxxxxx }"#
    );
}
