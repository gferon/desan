use std::fmt::{Debug, Error, Formatter};

mod string;
pub use crate::string::SanitizedString;

#[cfg(feature = "uuid")]
mod uuid;
#[cfg(feature = "uuid")]
pub use crate::uuid::SanitizedUuid;

pub use desan_derive::SanitizeDebug;

pub trait SanitizeDebug {
    fn fmt_sanitized(&self, fmt: &mut Formatter<'_>) -> Result<(), Error>;
}

pub trait Sanitizable<'s, Args> {
    type Sanitized: Debug;

    fn sanitize(&'s self, args: Args) -> Self::Sanitized;
}
